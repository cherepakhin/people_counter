package ru.perm.v.counter.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.perm.v.counter.domain.entity.CounterValue;

public interface CounterValueRepository extends CrudRepository<CounterValue, Long> {

}
