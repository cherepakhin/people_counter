package ru.perm.v.counter.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.perm.v.counter.domain.entity.Counter;

public interface CounterRepository extends CrudRepository<Counter, Long> {

}
