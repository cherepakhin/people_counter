package ru.perm.v.counter.controllers;

import java.util.List;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.perm.v.counter.criteries.CounterCritery;
import ru.perm.v.counter.domain.dto.CounterDTO;
import ru.perm.v.counter.domain.dto.CountersDTO;
import ru.perm.v.counter.domain.entity.Counter;
import ru.perm.v.counter.services.CounterService;

@RestController
@RequestMapping("/counter")
public class CounterController {

  private final ModelMapper modelMapper;
  public Logger logger = LoggerFactory.getLogger(CounterController.class);
  private CounterService counterService;

  @Autowired
  public CounterController(CounterService counterService, ModelMapper modelMapper) {
    this.counterService = counterService;
    this.modelMapper = modelMapper;
  }

  @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public ResponseEntity<CounterDTO> getById(@PathVariable(name = "id") Long id) {
    logger.info("Get Counter id=" + id);
    final Counter counter = counterService.getById(id);
    final CounterDTO dto = this.modelMapper.map(counter, CounterDTO.class);
    return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON_UTF8).body(dto);
  }

  @RequestMapping(value = "", method = RequestMethod.POST)
  public ResponseEntity<CounterDTO> create(@RequestBody Counter o) throws Exception {
    logger.info("Create Counter:" + o);
    Counter counter = counterService.save(o);
    logger.info("Successfully created:" + counter);
    final CounterDTO dto = this.modelMapper.map(counter, CounterDTO.class);
    return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON_UTF8).body(dto);
  }

  @RequestMapping(value = "/v-{search}", method = RequestMethod.POST)
  public ResponseEntity<CountersDTO> search(@PathVariable String search,@RequestBody CounterCritery critery) throws Exception {
    System.out.println("Search counters:" + search);
    logger.info("Search counters:" + critery);
    List<Counter> counters = counterService.getByCritery(critery);
    CountersDTO dto = new CountersDTO(counters);
    return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON_UTF8).body(dto);
  }
}
