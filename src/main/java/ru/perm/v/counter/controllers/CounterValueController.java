package ru.perm.v.counter.controllers;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.perm.v.counter.criteries.CounterValueCritery;
import ru.perm.v.counter.domain.dto.CounterValueDTO;
import ru.perm.v.counter.domain.dto.CounterValuesDTO;
import ru.perm.v.counter.domain.entity.CounterValue;
import ru.perm.v.counter.services.CounterValueService;

@RestController
@RequestMapping("/counter-value")
public class CounterValueController {

  private final ModelMapper modelMapper;
  private CounterValueService counterValueService;
  public Logger logger = LoggerFactory.getLogger(CounterValueController.class);

  @Autowired
  public CounterValueController(CounterValueService counterValueService, ModelMapper modelMapper) {
    this.counterValueService = counterValueService;
    this.modelMapper = modelMapper;
  }

  @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public ResponseEntity<CounterValueDTO> getById(@PathVariable(name = "id") Long id) {
    logger.info("Get Counter id=:" + id);
    final CounterValue counterValue = counterValueService.getById(id);
    final CounterValueDTO dto = this.modelMapper.map(counterValue, CounterValueDTO.class);
    return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON_UTF8).body(dto);
  }

  @RequestMapping(value = "/search", method = RequestMethod.POST)
  public ResponseEntity<CounterValuesDTO> search(@RequestBody String json) throws Exception {
    logger.info("Search CounterValue:" + json);
    List<CounterValue> values = counterValueService
        .getByCritery(new CounterValueCritery());
    return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON_UTF8).body(new CounterValuesDTO(values));
  }

  @RequestMapping(value = "/increase", method = RequestMethod.POST)
  public ResponseEntity<String> increase(HttpServletRequest request) throws Exception {
    String ip = request.getRemoteAddr();
    logger.info("Increase counter IP=" + ip);
    counterValueService.increase(ip);
    return ResponseEntity.ok().body("Ok");
  }
}
