package ru.perm.v.counter.domain.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import ru.perm.v.counter.domain.entity.CounterValue;

public class CounterValuesDTO {

  private List<CounterValueDTO> counterValues = new ArrayList<CounterValueDTO>();

  public CounterValuesDTO() {
  }

  public CounterValuesDTO(List<CounterValue> _values) {
    if (_values != null) {
      counterValues = _values.stream().map(CounterValueDTO::new).collect(Collectors.toList());
    }
  }

  public List<CounterValueDTO> getCounterValues() {
    return counterValues;
  }

  public void setCounterValues(List<CounterValueDTO> counterValues) {
    this.counterValues = counterValues;
  }

  @Override
  public String toString() {
    return "CounterValuesDTO{" +
        "counterValues=" + counterValues +
        '}';
  }
}
