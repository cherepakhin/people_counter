package ru.perm.v.counter.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDateTime;
import ru.perm.v.counter.domain.entity.Counter;
import ru.perm.v.counter.domain.entity.CounterValue;

public class CounterValueDTO {

  private Long id;
  private Integer val = 0;
  private Counter counter;
  @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
  private LocalDateTime ddate = LocalDateTime.now();

  public CounterValueDTO() {
  }

  public CounterValueDTO(CounterValue counterValue) {
    this(counterValue.getVal(), counterValue.getCounter(), counterValue.getDdate());
  }

  public CounterValueDTO(Integer val, Counter counter, LocalDateTime ddate) {
    this.val = val;
    this.counter = counter;
    this.ddate = ddate;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Integer getVal() {
    return val;
  }

  public void setVal(Integer val) {
    this.val = val;
  }

  public Counter getCounter() {
    return counter;
  }

  public void setCounter(Counter counter) {
    this.counter = counter;
  }

  public LocalDateTime getDdate() {
    return ddate;
  }

  public void setDdate(LocalDateTime ddate) {
    this.ddate = ddate;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof CounterValueDTO)) {
      return false;
    }

    CounterValueDTO that = (CounterValueDTO) o;

    return id != null ? id.equals(that.id) : that.id == null;
  }

  @Override
  public int hashCode() {
    int result = id != null ? id.hashCode() : 0;
    return result;
  }

  @Override
  public String toString() {
    return "CounterValueDTO{" +
        "id=" + id +
        ", val=" + val +
        ", counter=" + counter +
        ", ddate=" + ddate +
        '}';
  }
}
