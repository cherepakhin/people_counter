package ru.perm.v.counter.domain.entity;

import static javax.persistence.GenerationType.AUTO;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDateTime;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class CounterValue {

  @Id
  @GeneratedValue(strategy = AUTO)
  protected Long id;

  private Integer val = 0;

  @ManyToOne
//      (fetch = FetchType.EAGER)
  @JoinColumn(name = "counter_id", nullable = false)
  @Fetch(FetchMode.JOIN)
  private Counter counter;

  @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
  private LocalDateTime ddate = LocalDateTime.now();

  public CounterValue() {
  }

  public CounterValue(Integer val, Counter counter, LocalDateTime ddate) {
    this.val = val;
    this.counter = counter;
    this.ddate = ddate;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Integer getVal() {
    return val;
  }

  public void setVal(Integer val) {
    this.val = val;
  }

  public Counter getCounter() {
    return counter;
  }

  public void setCounter(Counter counter) {
    this.counter = counter;
  }

  public LocalDateTime getDdate() {
    return ddate;
  }

  public void setDdate(LocalDateTime ddate) {
    this.ddate = ddate;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof CounterValue)) {
      return false;
    }

    CounterValue that = (CounterValue) o;

    return id.equals(that.id);
  }

  @Override
  public int hashCode() {
    int result = val != null ? val.hashCode() : 0;
    result = 31 * result + (counter != null ? counter.hashCode() : 0);
    result = 31 * result + (ddate != null ? ddate.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "CounterValue{" +
        "id=" + id +
        ", val=" + val +
        ", counter=" + counter +
        ", ddate=" + ddate +
        '}';
  }
}
