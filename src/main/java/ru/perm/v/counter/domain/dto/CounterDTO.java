package ru.perm.v.counter.domain.dto;

import java.util.List;
import ru.perm.v.counter.domain.entity.Counter;

public class CounterDTO {

  private Long id;
  private String ip;
  private String name;

  public CounterDTO() {
  }

  public CounterDTO(Counter counter) {
    this(counter.getId(), counter.getIp(), counter.getName());
  }

  public CounterDTO(Long id, String ip, String name) {
    this.id = id;
    this.ip = ip;
    this.name = name;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getIp() {
    return ip;
  }

  public void setIp(String ip) {
    this.ip = ip;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof CounterDTO)) {
      return false;
    }

    CounterDTO that = (CounterDTO) o;

    if (id != null ? !id.equals(that.id) : that.id != null) {
      return false;
    }
    if (ip != null ? !ip.equals(that.ip) : that.ip != null) {
      return false;
    }
    return name != null ? name.equals(that.name) : that.name == null;
  }

  @Override
  public int hashCode() {
    int result = id != null ? id.hashCode() : 0;
    result = 31 * result + (ip != null ? ip.hashCode() : 0);
    result = 31 * result + (name != null ? name.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "CounterDTO{" +
        ", id=" + id +
        ", name='" + name + '\'' +
        "ip='" + ip + '\'' +
        '}';
  }
}
