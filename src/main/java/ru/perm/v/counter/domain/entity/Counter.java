package ru.perm.v.counter.domain.entity;

import static javax.persistence.GenerationType.AUTO;

import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Описание счетчика
 */
@Entity
public class Counter {

  @Id
  @GeneratedValue(strategy = AUTO)
  protected Long id;

  private String ip;
  private String name;

  public Counter() {
  }

  public Counter(Long id, String ip, String name) {
    this.id = id;
    this.name = name;
    this.ip = ip;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  /**
   * {@link ru.perm.v.counter.domain.entity.Counter#setIp}
   */
  public String getIp() {
    return ip;
  }

  /**
   * IP-адрес
   */
  public void setIp(String ip) {
    this.ip = ip;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Counter counter = (Counter) o;
    return id.equals(counter.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public String toString() {
    return "Counter{" +
        "id=" + id +
        ", name='" + name + '\'' +
        ", ip='" + ip + '\'' +
        '}';
  }
}
