package ru.perm.v.counter.domain.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import ru.perm.v.counter.domain.entity.Counter;

public class CountersDTO {

  private List<CounterDTO> counters = new ArrayList<CounterDTO>();

  public CountersDTO() {
  }

  public CountersDTO(List<Counter> _counters) {
//    _counters.forEach(counter -> counters.add(new CounterDTO(counter)));
    if (_counters != null) {
      counters = _counters.stream().map(CounterDTO::new).collect(Collectors.toList());
    }
  }

  public List<CounterDTO> getCounters() {
    return counters;
  }

  public void setCounters(List<CounterDTO> counters) {
    this.counters = counters;
  }

  @Override
  public String toString() {
    return "CountersDTO{" +
        "counters=" + counters +
        '}';
  }
}
