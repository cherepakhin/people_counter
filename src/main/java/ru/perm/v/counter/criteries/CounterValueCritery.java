package ru.perm.v.counter.criteries;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDateTime;

/**
 * Критетрий поиска счетчика
 */
public class CounterValueCritery {

  @JsonFormat(pattern = "yyyy-MM-dd hh:mm")
  public LocalDateTime fromTime;
  @JsonFormat(pattern = "yyyy-MM-dd hh:mm")
  public LocalDateTime toTime;
  public CounterCritery counterCritery = new CounterCritery();

  @Override
  public String toString() {
    return "CounterValueCritery{" +
        "fromTime=" + fromTime +
        ", toTime=" + toTime +
        ", counterCritery=" + counterCritery +
        '}';
  }
}
