package ru.perm.v.counter.services.impl;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import ru.perm.v.counter.criteries.CounterCritery;
import ru.perm.v.counter.domain.entity.Counter;
import ru.perm.v.counter.domain.entity.QCounter;
import ru.perm.v.counter.repositories.CounterRepository;
import ru.perm.v.counter.services.ACrudService;
import ru.perm.v.counter.services.CounterService;


@Service("counterService")
@Repository
@Transactional
public class CounterServiceImpl extends ACrudService<Counter, Long> implements CounterService {

  EntityManager entityManager;

  @Autowired
  public CounterServiceImpl(CounterRepository counterRepository, EntityManager entityManager) {
    this.repository = counterRepository;
    this.entityManager = entityManager;
  }

  @Override
  public List<Counter> getByCritery(Object _critery) {
    JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);
    CounterCritery critery = (CounterCritery) _critery;
    QCounter qCounter = QCounter.counter;
    BooleanExpression expression = getBooleanExpressionByCritery(critery);

    JPAQuery<Counter> q = queryFactory.selectFrom(qCounter)
        .orderBy(qCounter.id.asc());
    if (expression != null) {
      q.where(expression);
    }
    List<Counter> counters = q.fetch();
    return counters;
  }

  @Override
  public BooleanExpression getBooleanExpressionByCritery(CounterCritery critery) {
    QCounter qCounter = QCounter.counter;
    List<BooleanExpression> predicates = new ArrayList<>();
    if (critery.ip != null && !critery.ip.isEmpty()) {
      predicates.add(qCounter.ip
          .containsIgnoreCase(critery.ip));
    }
    BooleanExpression expression = predicates.stream()
        .reduce((predicate, accum) -> accum.and(predicate))
        .orElse(null);
    return expression;
  }
}
