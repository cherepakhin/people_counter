package ru.perm.v.counter.services;

import ru.perm.v.counter.domain.entity.CounterValue;

public interface CounterValueService extends ICrudService<CounterValue, Long> {

  /**
   * Увеличить счетчик
   *
   * @param ip - ip адрес
   */
  void increase(String ip);
}
