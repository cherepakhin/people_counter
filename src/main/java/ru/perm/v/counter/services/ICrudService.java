package ru.perm.v.counter.services;

import java.io.Serializable;
import java.util.List;

public interface ICrudService<E, PK extends Serializable> {

  List<E> listAll();

  E getById(PK id);

  void delete(PK id);

  E save(E entity);

  List<E> getByCritery(Object critery);

}
