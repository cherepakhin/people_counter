package ru.perm.v.counter.services.impl;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.perm.v.counter.criteries.CounterValueCritery;
import ru.perm.v.counter.domain.entity.CounterValue;
import ru.perm.v.counter.domain.entity.QCounter;
import ru.perm.v.counter.domain.entity.QCounterValue;
import ru.perm.v.counter.repositories.CounterValueRepository;
import ru.perm.v.counter.services.ACrudService;
import ru.perm.v.counter.services.CounterService;
import ru.perm.v.counter.services.CounterValueService;

@Service("counterValueService")
@Transactional
public class CounterValueServiceImpl extends ACrudService<CounterValue, Long> implements
    CounterValueService {

  private final CounterService counterService;
  EntityManager entityManager;

  @Autowired
  public CounterValueServiceImpl(CounterValueRepository counterValueRepository,
      CounterService counterService, EntityManager entityManager) {
    this.repository = counterValueRepository;
    this.counterService = counterService;
    this.entityManager = entityManager;
  }

  @Override
  public List<CounterValue> getByCritery(Object _critery) {
    JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);
    CounterValueCritery critery = (CounterValueCritery) _critery;
    QCounterValue qCounterValue = QCounterValue.counterValue;
    JPAQuery<CounterValue> q = queryFactory.selectFrom(qCounterValue)
        .innerJoin(qCounterValue.counter, QCounter.counter)
        .orderBy(qCounterValue.counter.id.asc(), qCounterValue.ddate.asc());
    BooleanExpression counterExpression = counterService
        .getBooleanExpressionByCritery(critery.counterCritery);
    if (counterExpression != null) {
      q.on(counterExpression);
    }
    BooleanExpression expression = getBooleanExpressionByCritery(critery);
    if (expression != null) {
      q.on(expression);
    }
    List<CounterValue> values = q.fetch();
    return values;
  }

  private BooleanExpression getBooleanExpressionByCritery(CounterValueCritery critery) {
    QCounterValue qCounterValue = QCounterValue.counterValue;
    List<BooleanExpression> predicates = new ArrayList<>();
    if (critery != null) {
      if (critery.fromTime != null) {
        predicates.add(qCounterValue.ddate
            .goe(critery.fromTime));
      }
      if (critery.toTime != null) {
        predicates.add(qCounterValue.ddate
            .loe(critery.toTime));
      }
    }
    BooleanExpression expression = predicates.stream()
        .reduce((predicate, accum) -> accum.and(predicate)).orElse(null);
    return expression;
  }

  @Override
  public void increase(String ip) {

  }
}
