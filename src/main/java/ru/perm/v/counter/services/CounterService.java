package ru.perm.v.counter.services;

import com.querydsl.core.types.dsl.BooleanExpression;
import ru.perm.v.counter.criteries.CounterCritery;
import ru.perm.v.counter.domain.entity.Counter;

public interface CounterService extends ICrudService<Counter, Long> {

  /**
   * Получение условий отбора в формате для JPAQueryFactory
   * @param critery - Обект запроса
   * @return
   */
  BooleanExpression getBooleanExpressionByCritery(CounterCritery critery);
}
