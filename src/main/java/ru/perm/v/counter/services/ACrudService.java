package ru.perm.v.counter.services;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.springframework.data.repository.CrudRepository;

public abstract class ACrudService<E, PK extends Serializable> {

  protected CrudRepository<E, PK> repository;

  public List<E> listAll() {
    return (List<E>) repository.findAll();
  }

  public E getById(PK id) {
    return repository.findById(id).get();
  }

  public void delete(PK id) {
    repository.deleteById(id);
  }

  public E save(E o) {
    E ret = repository.save(o);
    return ret;
  }

  public List<E> iterableToList(Iterable<E> iterEntities) {
    List<E> entities = StreamSupport.stream(iterEntities.spliterator(), false)
        .collect(Collectors.toList());
    if (entities == null) {
      entities = new ArrayList<E>();
    }
    return entities;
  }

}
