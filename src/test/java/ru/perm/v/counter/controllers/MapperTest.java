package ru.perm.v.counter.controllers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.perm.v.counter.domain.dto.CounterDTO;
import ru.perm.v.counter.domain.entity.Counter;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MapperTest {

  @Autowired
  ModelMapper modelMapper;

  /**
   * Проверка работы ModelMapper
   */
  @Test
  public void mapperCounter() {
    Long ID = new Long(1000);
    String NAME = "NAME";
    String IP = "IP";

    Counter counter = new Counter(ID, IP, NAME);
    CounterDTO counterDTO = modelMapper.map(counter, CounterDTO.class);

    assert counter.getId().equals(counterDTO.getId());
    assert counter.getName().equals(counterDTO.getName());
    assert counter.getIp().equals(counterDTO.getIp());
  }
}
