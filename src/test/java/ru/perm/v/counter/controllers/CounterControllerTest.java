package ru.perm.v.counter.controllers;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import javax.transaction.Transactional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import ru.perm.v.counter.domain.entity.Counter;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@WebAppConfiguration
@AutoConfigureMockMvc
public class CounterControllerTest {

  private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
      MediaType.APPLICATION_JSON.getSubtype(),
      StandardCharsets.UTF_8);

  @Autowired
  private MockMvc mockMvc;

  @Test
  public void getById() throws Exception {
    Long ID = new Long(1001);
    // Данные для тесты берутся из import.sql
    mockMvc.perform(get("/counter/" + ID))
        .andExpect(status().isOk())
        .andExpect(content().contentType(contentType))
        .andExpect(jsonPath("$.id", is(ID.intValue())))
        .andExpect(jsonPath("$.name", is("test_ip1")))
        .andExpect(jsonPath("$.ip", is("1.1.1.1")));
  }

  @Test
  public void createCounter() throws Exception {
    String NAME = "NAME";
    String IP = "3.3.3.3";
    Counter counter = new Counter(null, IP, NAME);
    String clientJson = json(counter);

    this.mockMvc.perform(post("/counter")
        .contentType(contentType)
        .content(clientJson))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id", is(1)))
        .andExpect(jsonPath("$.name", is(NAME)))
        .andExpect(jsonPath("$.ip", is(IP)));
  }

  protected String json(Object o) throws IOException {
    ObjectMapper mapper = new ObjectMapper();
    return mapper.writeValueAsString(o);
  }

}