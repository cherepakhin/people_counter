package ru.perm.v.counter.repositories;

import java.net.UnknownHostException;
import javax.transaction.Transactional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.perm.v.counter.domain.entity.Counter;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class CounterRepositoryTest {

  @Autowired
  CounterRepository counterRepository;

  @Test
  public void create() throws UnknownHostException {
    Counter counter = new Counter(null, "192.168.1.1", "two");
    Counter newCounter = counterRepository.save(counter);
    assert counter.getName().equals(newCounter.getName());

    long count = counterRepository.count();
    assert count == 3;
  }

  @Test
  public void count() {
    long count = counterRepository.count();
    assert count == 2;
  }
}