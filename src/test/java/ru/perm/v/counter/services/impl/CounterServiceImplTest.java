package ru.perm.v.counter.services.impl;

import java.util.List;
import javax.transaction.Transactional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.perm.v.counter.criteries.CounterCritery;
import ru.perm.v.counter.domain.entity.Counter;
import ru.perm.v.counter.services.CounterService;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class CounterServiceImplTest {

  @Autowired
  CounterService counterService;

  @Test
  public void getById() {
    Long ID = new Long(1001);
    Counter counter = counterService.getById(ID);
    assert counter.getId().equals(ID);
  }

  @Test
  public void getByCritery() {
    CounterCritery critery = new CounterCritery();
    critery.ip = "1.1.1.1";
    List<Counter> counters = counterService.getByCritery(critery);
    assert counters.size() == 1;
    assert counters.get(0).getIp().equals(critery.ip);
  }

  @Test
  public void getByCriterySort() {
    CounterCritery critery = new CounterCritery();
    List<Counter> counters = counterService.getByCritery(critery);
    assert counters.size() == 2;
    assert counters.get(0).getId().equals(new Long(1001));
    assert counters.get(1).getId().equals(new Long(1002));
  }
}