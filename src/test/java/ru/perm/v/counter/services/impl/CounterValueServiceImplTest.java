package ru.perm.v.counter.services.impl;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.List;
import javax.transaction.Transactional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.perm.v.counter.criteries.CounterValueCritery;
import ru.perm.v.counter.domain.entity.Counter;
import ru.perm.v.counter.domain.entity.CounterValue;
import ru.perm.v.counter.services.CounterService;
import ru.perm.v.counter.services.CounterValueService;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class CounterValueServiceImplTest {

  @Autowired
  CounterValueService counterValueService;

  @Autowired
  CounterService counterService;

  @Test
  public void create() {
    Long COUNTER_ID = new Long(1001);
    Counter counter = counterService.getById(COUNTER_ID);
    CounterValue counterValue = new CounterValue();
    counterValue.setCounter(counter);
    Integer VAL = 1;
    counterValue.setVal(VAL);
    counterValue = counterValueService.save(counterValue);
    assert counterValue.getCounter().equals(counter);
    assert counterValue.getVal().equals(VAL);
  }

  @Test
  public void getByCriteryIpAddresss() {
    String IP = "1.1.1.1";
    CounterValueCritery valueCritery = new CounterValueCritery();
    valueCritery.counterCritery.ip = IP;
    List<CounterValue> values = counterValueService.getByCritery(valueCritery);
    Long ID_1 = new Long(1001);
    LocalDateTime ddate = LocalDateTime.of(2019, Month.AUGUST, 4, 1, 0, 0);
    assert values.size() == 2;
    assert values.get(0).getCounter().getId().equals(ID_1);
    assert values.get(0).getCounter().getIp().equals(IP);
    assert values.get(0).getDdate().equals(
        LocalDateTime.of(2019, Month.AUGUST, 4, 1, 0, 0));

    assert values.get(1).getCounter().getId().equals(ID_1);
    assert values.get(1).getCounter().getIp().equals(IP);
    assert values.get(1).getDdate().equals(
        LocalDateTime.of(2019, Month.AUGUST, 4, 3, 0, 0));
  }

  @Test
  public void getByCriteryFromTime() {
    CounterValueCritery valueCritery = new CounterValueCritery();
    LocalDateTime FROM_TIME = LocalDateTime.of(2019, Month.AUGUST, 4, 2, 0, 0);
    valueCritery.fromTime = FROM_TIME;
    List<CounterValue> values = counterValueService.getByCritery(valueCritery);
    Long ID_1 = new Long(1001);
    Long ID_2 = new Long(1002);
    assert values.size() == 2;
    assert values.get(0).getCounter().getId().equals(ID_1);
    assert values.get(0).getDdate().equals(
        LocalDateTime.of(2019, Month.AUGUST, 4, 3, 0, 0));

    assert values.get(1).getCounter().getId().equals(ID_2);
    assert values.get(1).getDdate().equals(
        LocalDateTime.of(2019, Month.AUGUST, 4, 2, 0, 0));
  }
}