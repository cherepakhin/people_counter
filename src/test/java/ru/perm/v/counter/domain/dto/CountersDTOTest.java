package ru.perm.v.counter.domain.dto;

import org.junit.Test;

public class CountersDTOTest {

  @Test
  public void forNull() {
    CountersDTO dto = new CountersDTO(null);
    assert dto.getCounters().size() == 0;
  }
}