set schema sa;
insert into counter(id,ip,name) values(1001,'1.1.1.1','test_ip1');
insert into counter(id,ip,name) values(1002,'2.2.2.2','test_ip2');

insert into counter_value(id, ddate,val,counter_id) values(1,'2019-08-04 01:00:00',11,1001);
insert into counter_value(id, ddate,val,counter_id) values(3,'2019-08-04 02:00:00',31,1002);
insert into counter_value(id, ddate,val,counter_id) values(2,'2019-08-04 03:00:00',21,1001);
